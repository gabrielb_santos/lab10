/**
* @file     tratamento.h
* @brief    Arquivo de cabeçalho contendo a definição das funções 
*           realizam o tratamento das entradas e alocações dinâmicas.
* @author   Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since    23/06/2017
* @date     25/06/2017
*/

#ifndef TRATAMENTO_H
#define TRATAMENTO_H 

#include "excecao.h"
#include "util.h"

namespace edb1{

	/** 
	* @brief	Função  que realiza o tratamento de exceções em
	*			realação ao tamanho passado.
	* @param	tam tamanho do vetor
	*/
	int erro_tamanho(int tam){
	 	Tamanho_invalido ex;
	 	util::limpa_buffer();
	 	try{

			if(tam <= 0 || tam >= 0x7fffffff) throw Tamanho_invalido();

			}catch (Tamanho_invalido &inv){
			 cerr << ex.what() << endl;
			 return 1;
			}catch(...) {
			 cerr << "\nErro desconhecido\n";
			 return 1;
		 	}

	 	return 0;
  	}


	/** 
	* @brief	Função template que realiza o tratamento de exceções em
	*			realação a alocação dinâmica.
	* @param	tam tamanho do vetor
	* @param	v vetor onde ocorrerá a alocação
	*/
	template<typename T>
	int erro_aloc(int tam, T *(&v))
	{
		Tamanho_invalido ex;
		try{
			v = new T[tam];
		} catch (bad_alloc &ex) {
			cerr << "Erro na alocação de memória: " << ex.what()  << "\nTente novamente\n";
			return 1;
			}

		return 0;
	}    


	/** 
	* @brief	Função template que realiza o tratamento de exceções 
	*			de forma completa.
	* @details	permite a alocação apenas se o tamanho passado for válido.
	* @param	tam tamanho do vetor
	* @param	array  onde ocorrerá a alteração
	*/
  	template<typename T>
  	void testa(int& tam, T *(&array))
  	{
		int erro = 1;
		int erro2 = 1;

		while(erro2 > 0)
		{
			cin >> tam;
			erro = erro_tamanho(tam);
			if(erro == 0)
				erro2 = erro_aloc(tam, array);
		}
  	}
}

#endif