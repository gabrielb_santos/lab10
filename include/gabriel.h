/**
* @file	 	gabriel.h
* @brief	Arquivo de cabeçalho usado para ligar todas os .h da biblioteca "gabriel" em um só.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	23/06/2017
* @date		25/06/2017
*/

#ifndef GABRIEL_H
#define GABRIEL_H 

#include <iostream>
#include <cstddef>

#include "buscas.h"
#include "ord.h"
#include "tratamento.h"
#include "lista.h"
#include "fila.h"
#include "pilha.h"

#endif