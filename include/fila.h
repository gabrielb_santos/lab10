/**
* @file	 	fila.h
* @brief	Arquivo de cabeçalho contendo a definição e implementação da classe Fila
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	23/06/2017
* @date		25/06/2017
*/

#ifndef FILA_H
#define FILA_H
#endif

namespace edb1 {

	template<typename T>
	class Fila {

		private:

			int tamanho; // tamanho da fila
			int inicio; // início da fila
			int max; // tamanho máximo da fila
			T *vetor; // vetor onde é implementado a fila

		public:

			Fila();
			Fila(int tam);
			~Fila();

			T get_frente();
			int get_tamanho();
			bool inserir_fim(T info);
			bool remover_inicio();
	};


	/** 
	 * @brief	construtor padrão
	 * @details instancia uma fila de tamanho 10
	 */
	template<typename T>
	Fila<T>::Fila() 
	{
		vetor = new T[10];
		max = 10;
		tamanho = 0;
		inicio = 0;
	}


	/** 
	 * @brief	construtor parametrizado
	 * @param 	tam tamaho da pilha
	 */
	template<typename T>
	Fila<T>::Fila(int tam) 
	{
		vetor = new T[tam];
		max = tam;
		tamanho = 0;
		inicio = 0;
	}


	/** 
	 * @brief	destrutor padrão
	 */
	template<typename T>
	Fila<T>::~Fila() {}


	/** 
	 * @brief	Método que retorna o elemento na primeria posição da fila
	 * @return 	primeiro elemento
	 */
	template<typename T>
	T Fila<T>::get_frente() 
	{
		return vetor[inicio];
	}


	/** 
	 * @brief	Método que retorna o tamanho da fila
	 * @return 	tamanho da fila
	 */
	template<typename T>
	int Fila<T>::get_tamanho()
	{
		return tamanho;
	}


	/** 
	 * @brief	Método que insere um elemento no inicio da fila
	 * @param	info elemento a ser inserido
	 * @return 	retrona false caso não seja possível inserir, e true caso contrário
	 */
	template<typename T>
	bool Fila<T>::inserir_fim(T info) 
	{
		if (!(tamanho < max)) return false;

		vetor[(inicio+tamanho)%max] = info;
		tamanho++;

		return true;
	}


	/** 
	 * @brief	Método que remove o primeiro elemento da fila
	 * @return 	retrona false caso não seja possível remover, e true caso contrário
	 */
	template<typename T>
	bool Fila<T>::remover_inicio()
	{
		if (tamanho == 0) return false;

		inicio = (inicio+1)%max;
		tamanho--;

		return true;
	}
}
