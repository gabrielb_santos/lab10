/**
* @file	 	excecao.h
* @brief	Arquivo de cabeçalho contendo a definição da classe Tamanho_invalido
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	23/06/2017
* @date		25/06/2017
*/

#ifndef EXCECAO_H
#define EXCECAO_H

#include <exception>
using std::exception;

namespace edb1
{
	class Tamanho_invalido : public exception
	{
		public:
			const char* what()//método herdado e alterado da classe exception.
			{
				return "O tamanho passado é inválido, tente novamente";
			}
		
	};
}

#endif