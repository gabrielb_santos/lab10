/**
* @file	    buscas.h
* @brief	Arquivo de cabeçalho contendo a definição das funções 
*			realizam a busca nas coleções passadas.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	23/06/2017
* @date		25/06/2017
*/

#ifndef BUSCAS_H
#define BUSCAS_H

#include <string>
using std::string;

namespace edb1
{

	/** 
	 * @brief	Algoritmo de busca sequencial iterativa
	 * @param	k valor procurado
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tam tamanho do vetor
	 * @return  posição do valor procurado, caso não exista no vetor, retorna -1
	 */
	template<typename T>
	int b_sequencial_i(T k, T *v, int tam)
	{
		int pos = 0;

		do{
			if (k == v[pos])
			{
				return pos;
			}

			pos++;

		}while(pos < tam);

		return -1;

	}


	/** 
	 * @brief	Algoritmo de busca sequencial recursiva
	 * @param	k valor procurado
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tam tamanho do vetor
	 * @return  posição do valor procurado, caso não exista no vetor, retorna -1
	 */
	template<typename T>
	int b_sequencial_r(T k, T *v, int tam)
	{
		if(tam == -1)
		{
			return -1;

		}else if(v[tam - 1] == k){

			return tam - 1;

		}else{

			return b_sequencial_r(k, v, tam - 1);
		}

	}


	/** 
	 * @brief	Algoritmo de busca binária iterativa
	 * @param	k valor procurado
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tamanho tamanho do vetor
	 * @return  posição do valor procurado, caso não exista no vetor, retorna -1
	 */
	template<typename T>
	int b_binaria_i(T k, T *v, int tamanho)
	{
		int inicio = 0, fim = tamanho-1, meio;

		while(inicio <= fim)
		{
			meio = (inicio + fim)/2;

			if(k < v[meio])
			{
				fim = meio - 1;

			}else if(k > v[meio]){

				inicio = meio + 1;

			}else {

				return meio;
			}
		}

		return - 1;
	
	}


	/** 
	 * @brief	Algoritmo de busca binária recursiva
	 * @param	k valor procurado
	 * @param	v vetor onde ocorrerá a busca
	 * @param	inicio indice do inicio da busca
	 * @param	fim indice do fim da busca
	 * @return  posição do valor procurado, caso não exista no vetor, retorna -1
	 */
	template<typename T>
	int b_binaria_r(T k, T *v, int inicio, int fim)
	{

		if(inicio > fim){
			return - 1;
		}

		int meio;
		meio = (inicio + fim)/2;

		if(k < v[meio])
		{
			return b_binaria_r(k, v, inicio, meio - 1);

		}else if(k > v[meio]){

			return b_binaria_r(k, v, meio + 1, fim);

		}else{

			return meio;
		} 
	}


	/** 
	 * @brief	Algoritmo de busca ternária iterativa
	 * @param	k valor procurado
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tamanho tamanho do vetor
	 * @return  posição do valor procurado, caso não exista no vetor, retorna -1
	 */
	template<typename T>
	int b_ternaria_i(T k, T *v, int tam)
	{
		int lo, hi;
		lo = 0;
		hi = tam-1;

		int mid1, mid2;

		while(lo <= hi)
		{
			mid1 = (lo + hi)/2;
			mid2 = 2 * mid1;

			if(k == v[mid1])
			{
				return mid1;

			}else if( k < v[mid1])
			{
				hi = mid1 - 1;

			}else if(k == v[mid2])
			{
				return mid2;

			}else if(k < v[mid2])
			{
				lo = mid1 + 1;
				hi = mid2 - 1;

			}else
			{
				lo = mid2 + 1;
			}

		}

		return -1;
	}


	/** 
	 * @brief	Algoritmo de busca binária recursiva
	 * @param	k valor procurado
	 * @param	v vetor onde ocorrerá a busca
	 * @param	inicio indice do inicio da busca
	 * @param	fim indice do fim da busca
	 * @return  posição do valor procurado, caso não exista no vetor, retorna -1
	 */
	template<typename T>
	int b_ternaria_r(T k, T *v, int inicio, int fim)
	{
	    int i;
	    int primeiro, segundo;
	    
	    if(inicio>fim)
	       return -1;

	    i= (fim - inicio)/3;
	    if(i==0) i++;

	    primeiro = i  + inicio -1;
	    segundo = i*2 + inicio - 1;

	    if(v[primeiro]==k)
	       return primeiro;
	    else
	    if(v[primeiro]>k)
	         return b_ternaria_r(k, v, inicio, primeiro-1);
	    else
	    {
	        if(v[segundo]==k)
	          return segundo;
	        else
	        if(v[segundo]>k)
	           return b_ternaria_r(k, v, primeiro+1,segundo-1);
	        else
	           return b_ternaria_r(k, v, segundo+1,fim);
	    }
	}

}

#endif