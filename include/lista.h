/**
* @file	 	lista.h
* @brief	Arquivo de cabeçalho contendo a definição e implementação da classe Lista
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	23/06/2017
* @date		25/06/2017
*/

#ifndef LISTA_H
#define LISTA_H

using std::cout;
using std::endl;

namespace edb1 {

	template<typename T>
	class Lista {

		private:

			int tamanho; // tamanhoda lista

			struct Nodo {

				T info;
				Nodo *next; // ponteiro para o próximo nodo
				Nodo *prev; // ponteiro para o nodo anterior
			};

			typename Lista<T>::Nodo *head; // ponteiro para o primerio elemento da lista
			typename Lista<T>::Nodo *tail; // ponteiro para o ultimo elemento da lista

			void achar_nodo(int indice, typename Lista<T>::Nodo *&nodo_auxiliar);
			void ligar_nodo(typename Lista<T>::Nodo *inserido, typename Lista<T>::Nodo *nodo_auxiliar);
				

		public:

			Lista();
			~Lista();

			T get_info(int indice);

			void inserir_indice(int indice, T info);
			void inserir_inicio(T info);
			void inserir_fim(T info);

			void remover_indice(int indice);
			void remover_inicio();
			void remover_fim();

			void printar_lista();
	};


	/** 
	 * @brief	construtor padrão
	 */
	template <typename T>
	Lista<T>::Lista()
	{
		tamanho = 0;
		head = new typename Lista<T>::Nodo;
		tail = new typename Lista<T>::Nodo;
		head->next = tail;
		tail->prev = head;
	}
	

	/** 
	 * @brief	destrutor padrão
	 */
	template <typename T>		
	Lista<T>::~Lista()
	{
		typename Lista<T>::Nodo *nodo_auxiliar = head->next;
		while ((nodo_auxiliar = nodo_auxiliar->next)) {
			delete nodo_auxiliar->prev;
		}
		delete head;
		delete tail;
	}


	/**
	* @brief 	Método que faz a ligação entre o nodo, nodo_auxiliar e nodo depois do nodo_auxiliar.
	* @param 	inserido elemento a ser inserido na lista
	* @param 	nodo_auxiliar nodo que auxilia na inserção no indice certo da lista
	*/
	template <typename T>
	void Lista<T>::ligar_nodo(typename Lista<T>::Nodo *inserido, typename Lista<T>::Nodo *nodo_auxiliar) 
	{
		inserido->prev = nodo_auxiliar->prev;
		nodo_auxiliar->prev->next = inserido;
		inserido->next = nodo_auxiliar;
		nodo_auxiliar->prev = inserido;
	}


	/**
	* @brief 	Método que encontra um nodo da lista de acordo com o indice passado
	* @param 	indice indice do elemento a ser buscado na lista
	* @param 	nodo_auxiliar nodo que auxilia na busca pelo elemento procurado.
	*/
	template <typename T>
	void Lista<T>::achar_nodo(int indice, typename Lista<T>::Nodo *&nodo_auxiliar) 
	{
		if (indice > tamanho/2) {
			nodo_auxiliar = tail->prev;
			for (int count = tamanho; count > indice; count--) {
			nodo_auxiliar = nodo_auxiliar->prev;
			}
		}
	}


	/**
	* @brief 	Método que obtem a informação de um nodo de acordo com o indice da lista passado
	* @param 	indice indice do elemento a ser buscado na lista
	* @return 	informção do elemento
	*/
	template <typename T>
	T Lista<T>::get_info(int indice) 
	{
		typename Lista<T>::Nodo *nodo_auxiliar;
		if (indice <= tamanho && indice > 0) {
			achar_nodo(indice, nodo_auxiliar);
		} else {
			cout << "\nNão foi possível o encontrar elemento informado. Posição ultrapassa o final da lista\n";
		}
			return nodo_auxiliar->info;
	}


	/**
	* @brief 	Método que insere a um nodo da lista de acordo com o indice passado
	* @param 	indice indice do elemento a ser buscado na lista
	* @param 	info nodo a ser adicionado na lista.
	*/
	template <typename T>
	void Lista<T>::inserir_indice(int indice, T info) 
	{
		if (indice > tamanho || indice < 0) {
			cout << "\nNão foi possível inserir o elemento. Posição ultrapassa o final da lista\n";
		return;
		}

		typename Lista<T>::Nodo *inserido = new typename Lista<T>::Nodo();
		inserido->info = info;
		inserido->next = NULL;
		inserido->prev = NULL;
		typename Lista<T>::Nodo *nodo_auxiliar;

		achar_nodo(indice, nodo_auxiliar);
		ligar_nodo(inserido, nodo_auxiliar);
		tamanho++;
	}


	/**
	* @brief 	Método que insere a um nodo no início da lista
	* @param 	info nodo a ser adicionado na lista.
	*/
	template <typename T>
	void Lista<T>::inserir_inicio(T info) 
	{
		typename Lista<T>::Nodo *inserido = new typename Lista<T>::Nodo();
		inserido->info = info;
		inserido->next = NULL;
		inserido->prev = NULL;
		typename Lista<T>::Nodo *h = this->head;

		h->next->prev = inserido;
		inserido->next = h->next;

		h->next = inserido;
		inserido->prev = h;

		tamanho++;
	}


	/**
	* @brief 	Método que insere a um nodo no final da lista
	* @param 	info nodo a ser adicionado na lista.
	*/
	template <typename T>
	void Lista<T>::inserir_fim(T info) 
	{
		typename Lista<T>::Nodo *inserido = new typename Lista<T>::Nodo();
		inserido->info = info;
		inserido->next = NULL;
		inserido->prev = NULL;
		typename Lista<T>::Nodo *t = this->tail;

		t->prev->next = inserido;
		inserido->prev = t->prev;

		t->prev = inserido;
		inserido->next = t;

		tamanho++;
	}


	/**
	* @brief 	Método que remove a um nodo da lista de acordo com o indice passado
	* @param 	indice indice do elemento a ser removido na lista
	*/
	template <typename T>
	void Lista<T>::remover_indice(int indice) 
	{
		if (indice > tamanho || indice < 0) {
			cout << "\nNão foi possível remover o elemento. Posição ultrapassa o final da lista\n";
			return;
		}

		typename Lista<T>::Nodo *nodo_auxiliar;

		achar_nodo(indice, nodo_auxiliar);
		typename Lista<T>::Nodo *tmp = nodo_auxiliar->next;
		tmp->prev = nodo_auxiliar->prev;
		tmp->prev->next = tmp;
		delete nodo_auxiliar;
		tamanho--;
	}

	
	/**
	* @brief 	Método que remove a um nodo no inicio da lista
	*/
	template <typename T>
	void Lista<T>::remover_inicio() 
	{
		if (head->next == tail) {
			cout << "\nA lista está vazia!\n";
			return;
		}
		typename Lista<T>::Nodo *tmp = head->next;
		head->next = tmp->next;
		tmp->next->prev = head;
		delete tmp;
		tamanho--;
	}
	

	/**
	* @brief 	Método que remove a um nodo no final da lista
	*/		
	template <typename T>
	void Lista<T>::remover_fim() 
	{
		if (tail->prev == head) {
			cout << "\nA lista está vazia!\n";
			return;
		}
		typename Lista<T>::Nodo *tmp = tail->prev;
		tail->prev = tmp->prev;
		tmp->prev->next = tail;
		delete tmp;
		tamanho--;
	}


	/**
	* @brief 	Método que imprime todos os elementos da lista
	*/
	template <typename T>
	void Lista<T>::printar_lista() 
	{
		typename Lista<T>::Nodo *runner = head;
		while((runner = runner->next)) {
			cout << runner->info << endl;
		}
	}
}

#endif