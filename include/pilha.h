/**
* @file	 	pilha.h
* @brief	Arquivo de cabeçalho contendo a definição e implementação da classe Pilha
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	23/06/2017
* @date		25/06/2017
*/

#ifndef PILHA_H
#define PILHA_H 

namespace edb1 {

	template<typename T>
	class Pilha {

		private:

			T *vector; // vetor onde é implementado a pilha
			int tam; // tamanho da pilha
			int max; // tamanho máximo da pilha

		public:

			Pilha();
			Pilha(int tam);
			~Pilha();

			T top();
			int get_tamanho();
			bool push(T n);
			bool pop();
			

	};


	/** 
	 * @brief	construtor padrão
	 * @details instancia uma pilha de tamanho 10
	 */
	template<typename T>
	Pilha<T>::Pilha() 
	{
		vector = new T[10];
		max = 10;
		tam = 0;
	}

	/** 
	 * @brief	construtor parametrizado
	 * @param 	n tamaho da pilha
	 */
	template<typename T>
	Pilha<T>::Pilha(int n) 
	{
		vector = new T[n];
		max = n;
		tam = 0;
	}


	/** 
	 * @brief	destrutor padrão
	 */
	template<typename T>
	Pilha<T>::~Pilha() {}


	/** 
	 * @brief	Método que retorna o elemento no topo da pilha
	 * @return 	último elemento
	 */
	template<typename T>
	T Pilha<T>::top() 
	{
		return vector[tam-1];
	}


	/** 
	 * @brief	Método que retorna o tamanho da pilha
	 * @return 	tamanho da pilha
	 */
	template<typename T>
	int Pilha<T>::get_tamanho() 
	{
		return tam;
	}


	/** 
	 * @brief	Método que insere um elemento no final da pilha
	 * @param	n N elemento a ser inserido
	 */
	template<typename T>
	bool Pilha<T>::push(T n) 
	{
		if (!(tam < max)) return false;
		vector[tam] = n;
		tam++;
		return true;
		
	}


	/** 
	 * @brief	Método que deleta o último elemento da pilha
	 */
	template<typename T>
	bool Pilha<T>::pop() 
	{
		if (tam > 0) {
			tam--;
			return true;
		}
		return false;
	}

}

#endif