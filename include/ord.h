/**
* @file	    ord.h
* @brief	Arquivo de cabeçalho contendo a definição das funções 
*			realizam a ordenação nas coleções passadas.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	23/06/2017
* @date		25/06/2017
*/

#ifndef ORD_H
#define ORD_H

namespace edb1
{

	/** 
	 * @brief	Função que retorna a posição do menor elemento do vetor
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tam tamanho do vetor
	 * @param	aux variável que recebe a posição do menor elemento do vetor
	 * @return  posição do meno elemento do vetor 
	 */
	template<typename T>
	int min_i(T *v, int tam, int aux)
	{

		T m = v[aux];

		int pos = aux;

		for (int i = aux; i < tam; ++i)
		{
			if(v[i] < m)
			{
				m = v[i];
				pos = i;
			}
		}

		return pos;
	}


	/** 
	 * @brief	Algoritmo de ordenação selection sort
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tam tamanho do vetor
	 */
	template<typename T>
	void select_sort(T *v, int tam)
	{
		int min = 0;

		T aux;

		for (int i = 0; i < tam; ++i)
		{
			min = min_i(v, tam, i);

			aux = v[i];
			v[i] = v[min];
			v[min] = aux;
		}
	}


	/** 
	 * @brief	Algoritmo de ordenação insertion sort
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tam tamanho do vetor
	 */
	template<typename T>
	void insert_sort(T *v, int tamanho)
	{
      int i, j;
      T tmp;
      for (i = 1; i < tamanho; i++)
       {
            j = i;

            while (j > 0 && v[j - 1] > v[j])
            {
                  tmp = v[j];
                  v[j] = v[j - 1];
                  v[j - 1] = tmp;
                  j--;
            }
      	}
    }


	/** 
	 * @brief	Algoritmo de ordenação bubble sort
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tam tamanho do vetor
	 */
    template<typename T>
    void bubble_sort(T *v, int tam)
    {
    	int n = tam;

    	int j;
    	bool troca;
    	T aux;
 		j = n - 2;

    	do{

    		troca = false;
    		for(int i = 0; i <= j; ++i)
    		{
	    		if(v[i] > v[i + 1])
	    		{
	    			aux = v[i];
	    			v[i] = v[i + 1];
	    			v[i + 1] = aux;

	    			troca = true;
	    		}
	    	}

    		j--;

    	}while(troca);
    }


	/** 
	 * @brief	Algoritmo de ordenação quick sort
	 * @param	v vetor onde ocorrerá a busca
	 * @param	esq indice do vetor onde se iniciará a ordenação
	 * @param	dir indice do vetor onde se finalizará a ordenação
	 */
	template <typename T> 
	void quick_sort(T *v, int esq, int dir)
	 {
		int left1 = esq, right1 = dir;
		T pivot = v[(esq + right1) / 2];

		while(left1 <= right1) {

			while(v[left1] < pivot) left1++;
			while(v[right1] > pivot) right1--;

			if(left1 <= right1){

				T tmp = v[left1];
				v[left1] = v[right1];
				v[right1] = tmp;
				left1++;
				right1--;
			}
		}

		if (esq < right1) quick_sort(v,esq,right1);
		if (left1 < dir) quick_sort(v,left1,dir);
	}


	/** 
	 * @brief	Função que auxilia o merge sort na divisão e ordenação do vetor
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tam tamanho do vetor
	 */
	template <typename T>
	void intercall(T *v, int tam, int k)
	{
		int p = 0;
		int q = k;
		T *vtmp = new T[tam];
		int s =0 ;
		while(p<k && q<tam) {
			if (v[p]<=v[q]) vtmp[s++]=v[p++];
			else vtmp[s++]=v[q++];
		}
		while (p<k) vtmp[s++] = v[p++];
		while (q<tam) vtmp[s++] = v[q++];
		for (int i = 0; i < tam; i++) {
			v[i] = vtmp[i];	
		} 
	}


	/** 
	 * @brief	Algoritmo de ordenação merge sort
	 * @param	v vetor onde ocorrerá a busca
	 * @param	tam tamanho do vetor
	 */
	template <typename T>
	void merge_sort(T *v, int tam)
	{
		if (tam <= 1) return;
		int k = tam/2;
		merge_sort(v, k);
		merge_sort(&v[k], tam-k);
		intercall(v, tam, k);
	}

}

#endif