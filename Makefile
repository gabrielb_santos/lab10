INC_DIR = include
SRC_DIR = src
OBJ_DIR = build
LIB_DIR = lib
BIN_DIR = bin
DOC_DIR = doc

CC = g++
CFLAGS = -Wall -pedantic -std=c++11 -ansi -I. -I$(INC_DIR)

.PHONY: init linux util.a util.so gabriel.a gabriel.so prog_estatico prog_dinamico doxy clean din est


linux: util.a util.so gabriel.a gabriel.so prog_estatico prog_dinamico

#LINUX

util.a:$(SRC_DIR)/util.cpp  $(INC_DIR)/util.h 
	$(CC) $(CFLAGS) -c $(SRC_DIR)/util.cpp -o $(OBJ_DIR)/util.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/util.o
	@echo "\n\n+++ [Biblioteca estática criada em $(LIB_DIR)/$@] +++\n\n"

util.so: $(SRC_DIR)/util.cpp  $(INC_DIR)/util.h
	$(CC) $(CFLAGS) -fPIC -c $(SRC_DIR)/util.cpp -o $(OBJ_DIR)/util.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/util.o
	@echo "\n\n+++ [Biblioteca dinâmica criada em $(LIB_DIR)/$@] +++\n\n"


gabriel.a: $(SRC_DIR)/gabriel.cpp $(INC_DIR)/gabriel.h $(INC_DIR)/ord.h $(INC_DIR)/buscas.h $(INC_DIR)/lista.h $(INC_DIR)/fila.h $(INC_DIR)/pilha.h
	$(CC) $(CFLAGS) -c $(SRC_DIR)/gabriel.cpp -o $(OBJ_DIR)/gabriel.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/gabriel.o
	@echo "\n\n+++ [Biblioteca estática criada em $(LIB_DIR)/$@] +++\n\n"


gabriel.so: $(SRC_DIR)/gabriel.cpp $(INC_DIR)/gabriel.h $(INC_DIR)/ord.h $(INC_DIR)/buscas.h $(INC_DIR)/lista.h $(INC_DIR)/fila.h $(INC_DIR)/pilha.h
	$(CC) $(CFLAGS) -fPIC -c $(SRC_DIR)/gabriel.cpp -o $(OBJ_DIR)/gabriel.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/gabriel.o
	@echo "\n\n+++ [Biblioteca dinâmica criada em $(LIB_DIR)/$@] +++\n\n"


prog_estatico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/util.a $(LIB_DIR)/gabriel.a -o $(BIN_DIR)/$@

prog_dinamico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/util.so $(LIB_DIR)/gabriel.so -o $(BIN_DIR)/$@
	@echo "\n\n+++ MAKEFILE EXECUTADO COM SUCESSO +++\n\n"

clean:
	@rm -rf $(OBJ_DIR)/*
	@rm -rf $(DOC_DIR)/*
	@echo "\nRemovendo arquivos objeto, executaveis/binários e pasta HTML do diretório doc...\n"


doxy:
	@rm -rf $(DOC_DIR)/*
	doxygen Doxyfile



init:	
	@mkdir -p $(DOC_DIR)
	@mkdir -p $(BIN_DIR)
	@mkdir -p $(OBJ_DIR)
	@echo "\nDiretórios doc, bin e build criados\n"


din:
	./bin/prog_dinamico

est:
	./bin/prog_estatico