/**
* @file 	main.cpp
* @brief 	Programa que realiza testes sobre as TADS e algoritmos de busca
*			e ordenação, testando entradas e alocação de memória.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	23/06/2017
* @date		25/06/2017
*/

#include <string>
using std::string;

#include "gabriel.h"
using namespace edb1;

#include "util.h"
using namespace util;

int main() {
	limpa_tela();

	Lista <float> lista; //TADs alocadas para teste do template
	Fila <char> fila(10);//TADs alocadas para teste do template
	Pilha <int> pilha(5);//TADs alocadas para teste do template

	cout << "\n-------------TESTES-------------\n";
	cout << "\nInforme o tamanho da string a ser testada: ";

	string *a;
	int t_string;

	testa(t_string, a);

	cout << "\nDigite os elementos da string\n\n";

	for (int i = 0; i < t_string; ++i)
		getline(cin, a[i], '\n');


	cout << "\nInforme o tamanho do array de caracteres: ";
	int t_char;
	char *c;

	testa(t_char, c);

	cout << "\nDigite os elementos do Array\n";
	string input;

	for(int i = 0; i < t_char;)
	{
		getline(cin, input);
		if(input.length() != 1)
			cout << "\nAtenção! Digite apenas um caracter\n\n";
		else{
		   c[i] = input[0];
			i++;
		}
	}

	limpa_tela();


	cout << "\nInforme o tamanho do array de inteiros: ";
	int t_int;
	int *v;

	testa(t_int, v);

	cout << "\nDigite os elementos do Array\n";
	entra_elementos(v, t_int);



	cout << "\nInforme o tamanho do array de floats: ";
	int t_float;
	float *f;

	testa(t_float, f);

	cout << "\nDigite os elementos do Array\n";
	entra_elementos(f, t_float);



	limpa_tela();
	cout << "\n----------PRÉ-ORDENAÇÃO----------\n";

	cout << "String: ";
	print_array(a, t_string);

	cout << "\nVetor de caracteres: ";
	print_array(c, t_char);

	cout << "\nVetor de int: ";
	print_array(v, t_int);

	cout << "\nVetor de floats: ";
	print_array(f, t_float);


	cout << "\n----------PÓS-ORDENAÇÃO----------\n";
	merge_sort(v, t_int);
	quick_sort(a, 0, t_string - 1);
	insert_sort(f, t_float);
	select_sort(c, t_char);

	cout << "\nString (quick sort): ";
	print_array(a, t_string);

	cout << "\nVetor de caracteres (selection sort): ";
	print_array(c, t_char);

	cout << "\n\nVetor de int (merge sort): ";
	print_array(v, t_int);

	cout << "\nVetor de floats (insertion sort): ";
	print_array(f, t_float);

	cout << "\n--------------BUSCAS--------------\n\n";
	cout << "\nInforme a palavra a ser procurada na string: ";
	string m;
	cin.ignore();
	getline(cin, m, '\n');
	cout << "\nSequencial i. Pos. do elemento: " << b_sequencial_i(m, a, t_string);
	cout << "\nSequencial r. Pos. do elemento: " << b_sequencial_r(m, a, t_string);
	cout << "\n\nBinária i. Pos. do elemento: " << b_binaria_i(m, a, t_string);
	cout << "\nBinária r. Pos. do elemento: " << b_binaria_r(m, a, 0, t_string);
	cout << "\n\nTernária i. Pos. do elemento: " << b_ternaria_i(m, a, t_string);
	cout << "\nTernária r. Pos. do elemento: " << b_ternaria_r(m, a, 0, t_string) << endl;

	cout << "\n\nInforme o caracter a ser procurado no vetor de caracteres: ";
	char busca;
	int p = 0;
	while(p == 0)
	{
		getline(cin, input);
			if(input.length() != 1)
				cout << "\nAtenção! Digite apenas um caracter\n\n";
			else{
			   busca = input[0];
				p++;
			}
	}
	cout << "\nSequencial i. Pos. do elemento: " << b_sequencial_i(busca, c, t_char);
	cout << "\nSequencial r. Pos. do elemento: " << b_sequencial_r(busca, c, t_char);
	cout << "\n\nBinária i. Pos. do elemento: " << b_binaria_i(busca, c, t_char);
	cout << "\nBinária r. Pos. do elemento: " << b_binaria_r(busca, c, 0, t_char);
	cout << "\n\nTernária i. Pos. do elemento: " << b_ternaria_i(busca, c, t_char);
	cout << "\nTernária r. Pos. do elemento: " << b_ternaria_r(busca, c, 0, t_char) << endl;

	cout << "\n\nInforme o número que deseja procurar no vetor de inteiros: ";
	int n;
	invalida(n);
	
	cout << "\nSequencial i. Pos. do elemento: " << b_sequencial_i(n, v, t_int);
	cout << "\nSequencial r. Pos. do elemento: " << b_sequencial_r(n, v, t_int);
	cout << "\n\nBinária i. Pos. do elemento: " << b_binaria_i(n, v, t_int);
	cout << "\nBinária r. Pos. do elemento: " << b_binaria_r(n, v, 0, t_int);
	cout << "\n\nTernária i. Pos. do elemento: " << b_ternaria_i(n, v, t_int);
	cout << "\nTernária r. Pos. do elemento: " << b_ternaria_r(n, v, 0, t_int) << endl;


	cout << "\n\nInforme o número que deseja procurar no vetor de inteiros: ";
	float fbusca;
	invalida(fbusca);
	
	cout << "\nSequencial i. Pos. do elemento: " << b_sequencial_i(fbusca, f, t_float);
	cout << "\nSequencial r. Pos. do elemento: " << b_sequencial_r(fbusca, f, t_float);
	cout << "\n\nBinária i. Pos. do elemento: " << b_binaria_i(fbusca, f, t_float);
	cout << "\nBinária r. Pos. do elemento: " << b_binaria_r(fbusca, f, 0, t_float);
	cout << "\n\nTernária i. Pos. do elemento: " << b_ternaria_i(fbusca, f, t_float);
	cout << "\nTernária r. Pos. do elemento: " << b_ternaria_r(fbusca, f, 0, t_float) << endl;

	delete[] v;
	delete[] a;

	return 0;
	
	
}