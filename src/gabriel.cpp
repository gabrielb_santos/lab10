/**
* @file	 	gabriel.cpp
* @brief	Arquivo de corpo usado para a compilação dos .h da biblioteca "gabriel".
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	23/06/2017
* @date		25/06/2017
*/

#include <iostream>
using std::cout;

#include "gabriel.h"
